package com.example.dimaskomara.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class information extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
    }

    public void Telephone(View view) {
    }

    public void Home(View view) {
        Intent p = new Intent(information.this, MainActivity.class); // untuk bisa pindah ke activity pizzaABC dari activity menu
        startActivity(p);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void keluar(MenuItem item) {
        Intent intent = new Intent(information.this, Login.class);
        startActivity(intent);
    }

}
