package com.example.dimaskomara.helloworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class OrderReview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_review);
    }

    public void next(View view) {
        Intent intent = new Intent(this, finish_order.class);
        startActivity(intent);
    }

    public void back(View view) {
        Intent intent = new Intent(this, TATTHERING.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void keluar(MenuItem item) {
        Intent intent = new Intent(OrderReview.this, Login.class);
        startActivity(intent);
    }
}
