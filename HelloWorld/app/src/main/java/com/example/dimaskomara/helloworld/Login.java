package com.example.dimaskomara.helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    private EditText user;
    private EditText pswd;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = (EditText) findViewById(R.id.username);
        pswd = (EditText) findViewById(R.id.pass);
        login = (Button) findViewById(R.id.login);
    }

    public void login(View view) {
        if (user.getText().toString().equals("test")&& pswd.getText().toString().equals("123456") ) {
            Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show();
            Intent intentlogin = new Intent(this, HomeActivity.class);
            startActivity(intentlogin);
        } else {
            Toast.makeText(this,"Login Gagal", Toast.LENGTH_SHORT).show();
        }
    }
}
