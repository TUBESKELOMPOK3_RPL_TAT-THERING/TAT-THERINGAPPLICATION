package com.example.dimaskomara.helloworld;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private ArrayList<Home> mHomeData;
    private Context mContext;
    private GradientDrawable mGradientDrawable;


    HomeAdapter(Context context, ArrayList<Home> homeData) {
        this.mHomeData = homeData;
        this.mContext = context;

        mGradientDrawable = new GradientDrawable(); //membuat objek baru GradientDrwable
        mGradientDrawable.setColor(Color.GRAY);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mContext, LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false), mGradientDrawable);
    }

    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {
        Home currentHome = mHomeData.get(position);
        holder.bindTo(currentHome);

    }

    @Override
    public int getItemCount() {
        return mHomeData.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mTitleText;
        private TextView mInfoText;
        private ImageView mImage;
        private Home mCurrentMain;
        private Context mContext;
        private GradientDrawable mGradientDrawable;

        ViewHolder(Context context, View itemView, GradientDrawable gradientDrawable){
            super(itemView);

            mTitleText = (TextView) itemView.findViewById(R.id.title);
            mInfoText = (TextView) itemView.findViewById(R.id.subTitle);
            mImage = (ImageView) itemView.findViewById(R.id.homeImage);

            mContext = context ; //objek context
            mGradientDrawable = gradientDrawable;

            itemView.setOnClickListener(this);

        }
        void bindTo (Home currentHome){
            mTitleText.setText(currentHome.getTitle());
            mInfoText.setText(currentHome.getInfo());
            mCurrentMain = currentHome;

            Glide.with(mContext).load(currentHome.getImageResource()).into(mImage);
        }

        @Override
        public void onClick(View v) {
            Intent detailIntent = Home.starter(mContext, mCurrentMain.getTitle(),mCurrentMain.getImageResource());

            //Start the detail activity
            mContext.startActivity(detailIntent);
        }
    }
}
