package com.example.dimaskomara.helloworld;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ArrayList<Home> mHomeData;
    private HomeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mHomeData = new ArrayList<>();
        mAdapter = new HomeAdapter(this, mHomeData);
        mRecyclerView.setAdapter(mAdapter);

        initializeData();
    }
    private void initializeData(){
        String[] homeList = getResources().getStringArray(R.array.sports_titles);
        String[] homeInfo = getResources().getStringArray(R.array.sports_info);
        TypedArray imageResource = getResources().obtainTypedArray(R.array.sports_image);

        mHomeData.clear();

        for (int i=0; i<homeList.length;i++){
            mHomeData.add(new Home(homeList[i], homeInfo[i], imageResource.getResourceId(i,0)));
        }
        imageResource.recycle();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void keluar(MenuItem item) {
        Intent intent = new Intent(HomeActivity.this, Login.class);
        startActivity(intent);
    }

}