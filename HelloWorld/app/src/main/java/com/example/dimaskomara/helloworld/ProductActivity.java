package com.example.dimaskomara.helloworld;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        TextView mainTitle = (TextView) findViewById(R.id.titleDetail);
        ImageView mainImage = (ImageView) findViewById(R.id.ImageDetail);

        Drawable drawable = ContextCompat.getDrawable(this, getIntent().getIntExtra(Home.IMAGE_KEY, 0));
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(Color.GRAY);

        if (drawable != null) {
            gradientDrawable.setSize(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
        mainTitle.setText(getIntent().getStringExtra(Home.TITLE_KEY));

        Glide.with(this).load(getIntent().getIntExtra(Home.IMAGE_KEY, 0)).placeholder(gradientDrawable).into(mainImage);
    }
    public void next(View view) {
    }

    public void SateAyam(View view) {
        Intent intent = new Intent(this, TATTHERING.class);
        startActivity(intent);
    }

    public void NasiGoreng(View view) {
    }

    public void NasiHainan(View view) {
    }

    public void SotoAyam(View view) {
    }

    public void home(View view) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void keluar(MenuItem item) {
        Intent intent = new Intent(ProductActivity.this, Login.class);
        startActivity(intent);
    }
}
